import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Screen'),
      ),
      body: Container(
        child: Center(
          child: Container(
            height: 200,
            width: 200,
            child: Column(
              children: <Widget>[
                Text('You are logged in'),
                RaisedButton(
                  onPressed: () {
                    FirebaseAuth.instance.signOut();
                    Navigator.of(context).pop();

                    Navigator.of(context).pushReplacementNamed('/startingPage');
                  },
                  child: Text('Sign out'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
