import 'package:flutter/material.dart';

//Screen
import 'LoginScreen.dart';
import 'SignUpScreen.dart';
import 'HomeScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PushNamed Authentication',
      home: LoginScreen(),
      routes: <String, WidgetBuilder>{
        '/startingPage': (BuildContext context) => MyApp(),
        '/signUp': (BuildContext context) => SignUpScreen(),
        '/homeScreen': (BuildContext context) => HomeScreen(),
      },
    );
  }
}
